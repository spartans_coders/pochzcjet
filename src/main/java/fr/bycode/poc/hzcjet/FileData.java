package fr.bycode.poc.hzcjet;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class FileData implements Serializable {

    private Long lineNumber;
    private String sourceFileName;
    private String line;
    private String data[];

    public FileData parseLine() {
        data = line.split(",");
        line=null;
        return this;
    }
}