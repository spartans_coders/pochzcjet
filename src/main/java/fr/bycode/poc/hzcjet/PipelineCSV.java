package fr.bycode.poc.hzcjet;

import com.hazelcast.core.IMap;
import com.hazelcast.jet.IMapJet;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.pipeline.BatchSource;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sinks;
import com.hazelcast.jet.pipeline.Sources;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

import static java.nio.charset.StandardCharsets.UTF_8;

public class PipelineCSV implements Serializable {

    private Map<String, FileInfo> fileInfoMap;

    public Pipeline create() {
        String inputDir = "/Users/yblazart/Dev/work/";
        String inputfile="position-verybig.csv";

        Pipeline pipeline = Pipeline.create();

        fileInfoMap = new HashMap<>();

        //String inputDir = "/Users/yblazart/Dev/projects/bycode/pochzcjet/data";
        BatchSource<FileData> source = Sources.filesBuilder(inputDir)
                .charset(UTF_8)
                .glob(inputfile)
                .sharedFileSystem(true)
                .build((file, line) -> {
                    FileInfo fileInfo = fileInfoMap.computeIfAbsent(file, key -> {
                        System.out.println(" Start !!! " );
                        System.out.println(new Date());
                        return FileInfo.builder().lineCount(0L).sourceName(key).headers(line.split(",")).build();
                    }); // compute header
                    return FileData.builder().line(line).lineNumber(fileInfo.getAndIncrementLineCount()).sourceFileName(file).build();
                });


        pipeline
                .drawFrom(source)
                .filter(fd->fd.getLineNumber()>0) // skip header, managed in source
                .map(FileData::parseLine)   // parse in multi thread
                .map(fd-> {
                    fd.getData()[2]= "a";  // dummy replacement of value
                    return fd;
                })
//                .groupingKey(fd->"INV"+(fd.getLineNumber() % 50000))
//                .mapUsingIMap("bigmap",(fd,lst) -> {  // simulate high compute cpu
//                    return Cpu.test(fd,(List<String[]>) lst);
//
//                })
                .mapUsingReplicatedMap("bigmap",(rmap,fd)->Cpu.test(fd,rmap))
                .drainTo(Sinks.noop());
        return pipeline;
    }


}
