package fr.bycode.poc.hzcjet;

import com.hazelcast.config.Config;
import com.hazelcast.config.InMemoryFormat;
import com.hazelcast.config.NearCacheConfig;
import com.hazelcast.config.SerializerConfig;
import com.hazelcast.core.ReplicatedMap;
import com.hazelcast.jet.IMapJet;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.config.JetConfig;
import com.hazelcast.jet.pipeline.BatchSource;
import com.hazelcast.jet.pipeline.Sources;
import com.hazelcast.jet.server.JetBootstrap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.nio.charset.StandardCharsets.UTF_8;

public class PocJet {


    public static void main(String[] args) {


        SerializerConfig serializerConfig = new SerializerConfig()
                .setImplementation(new FileDataSer())
                .setTypeClass(FileData.class);

        JetConfig jetConfig=new JetConfig();
        jetConfig.getHazelcastConfig().getSerializationConfig()
                .addSerializerConfig(serializerConfig);
        jetConfig.getHazelcastConfig().getMapConfig("bigmap")
                .setNearCacheConfig(
                        new NearCacheConfig()
                                .setInMemoryFormat(InMemoryFormat.OBJECT)
                                .setCacheLocalEntries(true));

        jetConfig.getHazelcastConfig().getReplicatedMapConfig("bigmap")
                .setInMemoryFormat(InMemoryFormat.OBJECT);

        int nbInstances=1;
        // create 4 instances;
        List<JetInstance> jetInstances = IntStream.range(0, nbInstances).mapToObj(n -> Jet.newJetInstance(jetConfig)).collect(Collectors.toList());

        try {
            // create jet client
//            JetInstance jetClient = JetBootstrap.getInstance();
            JetInstance jetClient = Jet.newJetClient();


            System.out.println("Prepare big map");


            ReplicatedMap<String, List<String[]>> bigMap = jetClient.getReplicatedMap("bigmap");


            Cpu.fill(bigMap);

            System.out.println(" Big map done" );

            long begin=System.currentTimeMillis();
            jetClient.newJob(new PipelineCSV().create()).join();
            System.out.println(new Date());

            long total = System.currentTimeMillis() - begin;

            System.out.printf("End in "+total+" ms");

            jetClient.shutdown();
            // create pipeline
        } finally {
            Jet.shutdownAll();
        }

    }

}
