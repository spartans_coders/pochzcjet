package fr.bycode.poc.hzcjet;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class FileInfo  implements Serializable {

    private long lineCount;
    private String sourceName;

    private String headers[];

    public long getAndIncrementLineCount() {
        return lineCount++;
    }
}
