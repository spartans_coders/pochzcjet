package fr.bycode.poc.hzcjet;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.StreamSerializer;

import java.io.IOException;

public class FileDataSer implements StreamSerializer<FileData> {
    @Override
    public void write(ObjectDataOutput out, FileData object) throws IOException {
        out.writeLong(object.getLineNumber());
        out.writeUTF(object.getLine());
        out.writeUTFArray(object.getData());
        out.writeUTF(object.getSourceFileName());
    }

    @Override
    public FileData read(ObjectDataInput in) throws IOException {
        return FileData.builder()
                .lineNumber(in.readLong())
                .line(in.readUTF())
                .data(in.readUTFArray())
                .sourceFileName(in.readUTF())
                .build();
    }

    @Override
    public int getTypeId() {
        return 42;
    }

    @Override
    public void destroy() {

    }
}
