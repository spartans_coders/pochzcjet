package fr.bycode.poc.hzcjet.springpoopoo;

import fr.bycode.poc.hzcjet.Cpu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PositionItemProcessor implements ItemProcessor<Position, Position> {

    private static final Logger log = LoggerFactory.getLogger(PositionItemProcessor.class);

    public static Map<String, List<String[]>> bigMap=new HashMap<>();



    @Override
    public Position process(final Position person) throws Exception {

        return Cpu.test(person,bigMap);
    }

}
