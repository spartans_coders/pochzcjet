package fr.bycode.poc.hzcjet.springpoopoo;

import fr.bycode.poc.hzcjet.Cpu;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.sql.DataSource;
import java.util.List;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    // tag::readerwriterprocessor[]
    @Bean
    public FlatFileItemReader<Position> reader() {

        String inputDir = "/Users/yblazart/Dev/work/";
        String inputfile = "position-verybig.csv";
        return new FlatFileItemReaderBuilder<Position>()
                .name("personItemReader")
                .resource(new FileSystemResource(inputDir + inputfile))
                .delimited()
                .names("cnt,trCurr,amntTC,amntRC,baliId,mastId,univAcc,locGaapAcc,locGaap,flow,strategieias,douteux,sousprod,marche,stratcouv,detention,grossAmntRC,tierctpa,garantrc,correspondant,partner,tieremett,strategiefr,dureeini,eligib,cotation,posId,derivative,npeStRestruct,stage".split(","))
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Position>() {{
                    setTargetType(Position.class);
                }})
                .build();
    }

    @Bean
    public PositionItemProcessor processor() {
        Cpu.fill(PositionItemProcessor.bigMap);
        return new PositionItemProcessor();
    }

    @Bean
    public ItemWriter<Position> writer() {
        return new ItemWriter<Position>() {
            @Override
            public void write(List<? extends Position> list) throws Exception {

            }
        };
    }
    // end::readerwriterprocessor[]

    // tag::jobstep[]
    @Bean
    public Job importUserJob(Step step1) {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(ItemWriter<Position> writer) {
        return stepBuilderFactory.get("step1")

                .<Position, Position>chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(writer)
                .taskExecutor(taskExecutor())
                .throttleLimit(4)
                .build();
    }
    // end::jobstep[]

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(4);
        executor.setMaxPoolSize(4);
        executor.setThreadNamePrefix("default_task_executor_thread");
        executor.initialize();
        return executor;

    }
}
