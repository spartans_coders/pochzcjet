package fr.bycode.poc.hzcjet.springpoopoo;

import lombok.Data;

@Data
public class Position {

    private long lineNumber;
    private String cnt;
    private String trCurr;
    private String amntTC;
    private String amntRC;
    private String baliId;
    private String mastId;
    private String univAcc;
    private String locGaapAcc;
    private String locGaap;
    private String flow;
    private String strategieias;
    private String douteux;
    private String sousprod;
    private String marche;
    private String stratcouv;
    private String detention;
    private String grossAmntRC;
    private String tierctpa;
    private String garantrc;
    private String correspondant;
    private String partner;
    private String tieremett;
    private String strategiefr;
    private String dureeini;
    private String eligib;
    private String cotation;
    private String posId;
    private String derivative;
    private String npeStRestruct;
    private String stage;

}
