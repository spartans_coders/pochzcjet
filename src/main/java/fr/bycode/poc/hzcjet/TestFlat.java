package fr.bycode.poc.hzcjet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class TestFlat {

    public static void main(String[] args) throws IOException {


        System.out.println("Prepare big map");


        Map<String, List<String[]>> bigMap = new HashMap<>(50000);


        Cpu.fill(bigMap);

        System.out.println(" Big map done");


        long begin = System.currentTimeMillis();

        String inputDir = "/Users/yblazart/Dev/work/";

        File f = new File(inputDir, "position-verybig.csv");
        Map<String, FileInfo> fileInfoMap = new HashMap<>();
        AtomicLong ln = new AtomicLong();
        Files.lines(f.toPath()).forEach(line -> {
            FileInfo fileInfo = fileInfoMap.computeIfAbsent("a", key -> FileInfo.builder().lineCount(0L).sourceName(key).headers(line.split(",")).build()); // compute header
            FileData fd = FileData.builder().line(line).lineNumber(fileInfo.getAndIncrementLineCount()).sourceFileName("a").build();
            if (fd.getLineNumber() == 0) return;
            fd.parseLine();
            fd.getData()[2] = "a";


            long index = fd.getLineNumber() % 50000;
            Cpu.test(fd, bigMap);


        });


        long total = System.currentTimeMillis() - begin;

        System.out.printf("End in " + total + " ms");


    }
}
