package fr.bycode.poc.hzcjet;

import com.hazelcast.core.ReplicatedMap;
import fr.bycode.poc.hzcjet.springpoopoo.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Cpu {

    public static void fill(Map<String, List<String[]>> map) {
        for ( int index =0 ; index < 50000 ; index++ ) {
            List<String[]> dataList = new ArrayList<>(20);
            for ( int line = 0 ; line < 20 ; line ++) {
                String data[] = new String[15];
                dataList.add(data);
                for ( int dataIndex = 0 ; dataIndex < 15 ; dataIndex++) {
                    data[dataIndex] = "a"+dataIndex+"l"+line+"i"+index;
                }
            }
            map.put("INV"+index,dataList);
        }


    }

    public static FileData test(FileData fd, ReplicatedMap map) {
      return test(fd, (List<String[]>) map.get("INV"+(fd.getLineNumber() % 50000)))  ;
    }

    public static FileData test(FileData fd, Map<String, List<String[]>> map) {
      return test(fd,map.get("INV"+(fd.getLineNumber() % 50000)))  ;
    }

    public static FileData test(FileData fd, List<String[]> liste) {
        for (int index =0 ; index < 3 ; index ++ )
        for ( String [] data : liste ) {
            for ( int dataIndex = 0 ; dataIndex < 15 ; dataIndex ++ ) {
                if ( data[dataIndex].equals(index+""+fd.getData()[dataIndex])) {
                    System.out.println("GOTCHA !");
                }
            }
        }
        return fd;

    }

    private static String [] data = new String[15];

    public static Position test(Position position, Map<String, List<String[]>> bigMap) {
        FileData fd=new FileData(position.getLineNumber(),"a","",data);
        test(fd,bigMap);
        return position;
    }
}
